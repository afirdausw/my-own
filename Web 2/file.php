<?php include('header-white.php');
/*
        Template Name: testimoni
*/
?>
<!-- Breadcrumbs -->
<section class="user-navigation">
    <div class="copyright2">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-5 ">
                    <div class="tulisan3"> You are here: Home > Testimoni</div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Breadcrumbs -->

<!-- Testimonial Content -->
<section class="user-testimonial-content">
    <div class="kotak-testimoni">
        <div class="gambar">
            <div class="container">
                <br />
                <div class="row" id="detailvideo">
                    <br />
                    <!-- Sidebar -->
                    <div class="col-xs-hd col-md-4 col-lg-4 sb">
                        <aside>
                            <div class="sidebar-nav">
                                <div class="navbar" role="navigation">
                                    <div class="navbar-header">
                                        <div class="sidebar-title">
                                            <h4>Paket Kursus</h4>
                                        </div>
                                    </div>
                                    <div class="navbar-collapse collapse sidebar-navbar-collapse">
                                        <ul class="nav navbar-nav sidebartestimoni">
                                            <?php
                                                $args = array(
                                                    'menu' => 'sidebar',
                                                    'echo' => false
                                                );
                                                echo strip_tags(wp_nav_menu($args), '<li><a>');
                                                ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                    <!-- End Content of Sidebar -->

                    <!-- Video Testimoni Siswa -->
                    <div class="testimoni col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        <p class="tulisan6">BUKTIKAN BAHWA ANDA JUGA BAKAL MENJADI AHLINYA</p>
                        <div class="video-images">
                            <iframe class="video-responsive" width="100%" height="650px" src="https://www.youtube.com/embed/bkY4mqAwRXw?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                        <!--<div class="video-images">
                                <div class="btn-play"></div>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/video.jpg" height="453" width="801" alt="Responsive Image" class="img-responsive" >
                            </div>
                            <div class="video-aboutus">
                                 <img src="<?php echo get_template_directory_uri(); ?>/images/karisma2.gif" height="453" width="801" alt="Responsive Image" class="img-responsive" >
                            </div>-->
                        <p>
                            <div class="tulisana">
                                <strong>Sekarang</strong> adalah saat yang terbaik untuk bergabung dengan ribuan siswa
                                Kami yang puas. Kami adalah satu-satunya tempat kursus di Indonesia yang memberikan
                                pelayanan berbeda, memahami dan memberikan bekal keterampilan produktif yang Anda
                                butuhkan.
                            </div>
                        </p>
                    </div>
                    <br>
                    <!-- End Content of Video Testimoni Siswa -->
                </div>
            </div>
        </div>

        <div class="testi-content">
            <div class="container">
                <div class="container2">
                    <div class="col-md-12 testimonial">
                        <div class="hitam">
                            <?php
                                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                $args = array(
                                    'post_type' => 'student_testimony',
                                    'posts_per_page' => 5,
                                    'paged' => $paged
                                );
                                $wp_query = new WP_Query($args);
                                ?>
                            <?php if (have_posts()): while ($wp_query->have_posts()): $wp_query->the_post(); ?>
                            <div class="row garis">
                                <div class="tulisan7">
                                    <div class="col-md-2">
                                        <div id="nivo-lightbox-demo" class="video-thumbnail">
                                            <a data-lightbox-gallery="gallery1" href="<?php the_field('video_student'); ?>" class="videobutton">
                                                <div class="mini-video"></div>
                                                <img src="<?php the_field('photo'); ?>" alt="Responsive image" class="img-circle img-testi img-thumbnail img-responsive imghover">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div align="justify">
                                            <i style="font-size: 14.5px;">
                                                <?php the_field('testimony_description'); ?>
                                            </i>
                                        </div>
                                        <br /> <br />
                                        <font color="#056dd4" class="label-name"><b>
                                                <?php the_field('name'); ?>,
                                            </b></font>
                                        <span class="label label-warning label-margin">Join
                                            <?php the_field('join_date'); ?></span>
                                        <br />
                                        <span class="label-job">
                                            <?php the_field('job'); ?></span>
                                        <br /><br />
                                        <b>Materi Kursus yang saya ambil :</b>
                                        <!--                                        <ol start="angka">-->

                                        <?php the_field('subject_choosen'); ?>
                                    </div>
                                </div>
                            </div>
                            <?php endwhile; ?>
                            <br>
                            <br>
                            <div class="centeredcontent">
                                <?php
                                        wp_pagenavi();
                                        wp_reset_postdata();
                                        ?>
                            </div>
                            <?php
                                endif;
                                ?>
                            <!-- Paging -->
                            <!-- <div class="paging">
                                    <center>
                                        <div class="menuku-flickr">
                                            <ul class="big-paging">
                                                <li class="previous"><a href="#" >«</a></li>
                                                <li><a href="#">1</a></li>
                                                <li><a href="#">... </a></li>
                                                <li><a class="pagination-temp" href="#">4</a></li>
                                                <li><a href="#">5</a></li>
                                                <li class="active"><a href="#">6</a></li>
                                                <li><a href="#">7</a></li>
                                                <li><a href="#">8</a></li>
                                                <li><a href="#">9</a></li>
                                                <li><a href="#">10</a></li>
                                                <li><a href="#">11</a></li>
                                                <li><a href="#">12</a></li>
                                                <li><a href="#">13</a></li>
                                                <li><a href="#">...</a></li>
                                                <li><a href="#">89</a></li>
                                                <li><li class="next"><a href="#">»</a></li>
                                             </ul>
                                             <ul class="mobile-paging">
                                                <li class="previous"><a href="#" >«</a></li>
                                                <li><a href="#">1</a></li>
                                                <li><a href="#">... </a></li>
                                                <li class="active"><a href="#">5</a></li>
                                                <li><a href="#">7</a></li>
                                                <li><a href="#">9</a></li>
                                                <li><a href="#">...</a></li>
                                                <li><a href="#">89</a></li>
                                                <li><li class="next"><a href="#">»</a></li>
                                             </ul>
                                        </div>
                                    </center>
                                </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- -->
        <!-- </div> -->
    </div>
</section>
<?php get_footer() ?>
